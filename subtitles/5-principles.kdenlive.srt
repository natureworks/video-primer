1
00:00:00,300 --> 00:00:02,500
The fifth section is the key principles

2
00:00:02,500 --> 00:00:04,533
i think there are four key principles

3
00:00:04,533 --> 00:00:06,100
at play when you're creating a forest

4
00:00:06,100 --> 00:00:08,200
garden. The first one of those is

5
00:00:08,233 --> 00:00:10,700
working with nature and I think this  is really

6
00:00:10,700 --> 00:00:13,366
really important, you want to work with

7
00:00:13,366 --> 00:00:16,433
natural processes. For example here in West Wales

8
00:00:16,633 --> 00:00:20,133
the land would want really to be

9
00:00:20,666 --> 00:00:24,466
oak woodland so it's much easier to

10
00:00:24,466 --> 00:00:27,433
balance the garden on the edge of

11
00:00:27,433 --> 00:00:30,233
that woodland rather than have it absolutely

12
00:00:30,233 --> 00:00:32,066
clear and grow annual crops which is

13
00:00:32,100 --> 00:00:35,066
way loads more work. Martin Crawford's most

14
00:00:35,066 --> 00:00:37,500
excellent book 'Creating a Forest Garden'

15
00:00:38,300 --> 00:00:40,333
the subtitle is "Working with nature to

16
00:00:40,333 --> 00:00:44,500
grow edible crops" and basically what you're

17
00:00:44,500 --> 00:00:47,000
trying to do is create a balanced ecosystem

18
00:00:47,266 --> 00:00:49,466
and that's where the weed control, that's

19
00:00:49,466 --> 00:00:51,500
for the fertility in the garden and

20
00:00:51,500 --> 00:00:53,400
that's also for the pest control

21
00:00:53,400 --> 00:00:54,833
as well, because if you have a

22
00:00:54,866 --> 00:00:57,700
balanced ecosystem, all these things are balancing out

23
00:00:57,966 --> 00:01:00,333
and you're using the

24
00:01:00,633 --> 00:01:02,900
forces of nature to do the work

25
00:01:02,900 --> 00:01:05,866
for you. Obviously a large

26
00:01:05,866 --> 00:01:07,600
part of this is it's no-dig

27
00:01:07,666 --> 00:01:09,400
that you don't want to be churning

28
00:01:09,400 --> 00:01:11,866
up the soil all over. It's okay

29
00:01:11,866 --> 00:01:13,933
it creates extra habitat and you can

30
00:01:13,933 --> 00:01:16,366
grow annual vegetables in certain corners but

31
00:01:16,366 --> 00:01:17,900
not over the whole of the garden.

32
00:01:18,566 --> 00:01:19,666
It all makes it far more

33
00:01:19,666 --> 00:01:20,633
sustainable

34
00:01:20,933 --> 00:01:22,666
and lower maintenance as well

35
00:01:23,766 --> 00:01:25,400
The second principle for me is

36
00:01:25,400 --> 00:01:28,633
it's vegan, it's organic and it's natural. Industrial

37
00:01:28,633 --> 00:01:31,866
agriculture has a massive impact on carbon

38
00:01:31,866 --> 00:01:35,566
emissions and also on diseases as well

39
00:01:35,600 --> 00:01:37,366
just look at Covid and bird flu

40
00:01:37,933 --> 00:01:40,633
and we're much better off, there's no need to

41
00:01:40,633 --> 00:01:42,766
kill animals to grow food so if

42
00:01:42,766 --> 00:01:43,666
you don't have to

43
00:01:43,933 --> 00:01:45,600
I really don't think we should be killing

44
00:01:45,633 --> 00:01:48,966
animals. And organic because you don't really

45
00:01:48,966 --> 00:01:52,666
want to have pesticides and herbicides sprayed

46
00:01:52,666 --> 00:01:54,366
onto your food, it's not good for

47
00:01:54,366 --> 00:01:57,466
you. And natural, natural materials when you're

48
00:01:57,466 --> 00:02:00,566
growing, when you're creating the garden.

49
00:02:00,566 --> 00:02:04,066
Here is a recycled hoggin path

50
00:02:04,700 --> 00:02:08,866
in John Little's garden and you're reusing

51
00:02:08,866 --> 00:02:11,633
materials you're not digging up new materials

52
00:02:11,900 --> 00:02:13,933
and you're not using oil you're not

53
00:02:13,933 --> 00:02:18,666
creating really energy-intensive concrete so no plastic

54
00:02:18,866 --> 00:02:20,566
no oil or the absolute minimum

55
00:02:21,700 --> 00:02:26,100
Native plants where possible. Please

56
00:02:26,100 --> 00:02:28,666
if you're creating a garden use

57
00:02:28,666 --> 00:02:30,133
native plants where you can, where you

58
00:02:30,133 --> 00:02:31,633
don't have a food crop.

59
00:02:31,633 --> 00:02:33,633
For example, use them in bulk, use them

60
00:02:33,766 --> 00:02:36,500
in windbreaks and use them as ground cover plants

61
00:02:36,833 --> 00:02:38,266
and the reason for this is because

62
00:02:38,266 --> 00:02:41,766
native plants have co-evolved with wildlife

63
00:02:41,766 --> 00:02:44,766
and you create a more resilient ecosystem

64
00:02:44,833 --> 00:02:47,666
if you give those insects,

65
00:02:47,666 --> 00:02:50,533
give the invertebrates a food source, everything

66
00:02:50,533 --> 00:02:52,433
else can eat the invertebrates but that's

67
00:02:52,433 --> 00:02:55,600
far more resilient ecosystem than something with

68
00:02:56,066 --> 00:02:58,300
non-native plants that not so many insects

69
00:02:58,333 --> 00:03:00,466
will eat. You're supporting the food web

70
00:03:00,733 --> 00:03:01,933
There is a saying "If something

71
00:03:01,933 --> 00:03:03,433
is not eating your plants then

72
00:03:03,433 --> 00:03:04,666
your garden is not part of the

73
00:03:04,666 --> 00:03:07,233
ecosystem. So be happy when you see

74
00:03:07,233 --> 00:03:10,100
the holes in your plant leaves.

75
00:03:10,100 --> 00:03:11,966
The fourth principle is people are at

76
00:03:11,966 --> 00:03:14,733
the heart of this. It's really important

77
00:03:14,733 --> 00:03:17,100
to include people in these processes. For

78
00:03:17,100 --> 00:03:19,166
too long there has been a separation

79
00:03:19,433 --> 00:03:22,066
of people and the natural world

80
00:03:22,066 --> 00:03:24,333
We are part of the natural

81
00:03:24,333 --> 00:03:25,833
world and so we have to be

82
00:03:25,866 --> 00:03:27,166
included in that

83
00:03:28,366 --> 00:03:32,433
People tend the land, and rekindle

84
00:03:32,433 --> 00:03:35,333
that relationship with the world and this

85
00:03:35,333 --> 00:03:37,733
means looking at whenever you're creating

86
00:03:37,733 --> 00:03:39,700
a garden is to look at the

87
00:03:39,700 --> 00:03:42,000
management of the garden. Who will look

88
00:03:42,066 --> 00:03:44,500
after the garden long-term and what do

89
00:03:44,500 --> 00:03:45,766
they get from it and this is

90
00:03:45,766 --> 00:03:48,200
forest gardening is so brilliant because

91
00:03:48,200 --> 00:03:50,800
you get an edible crop you are far

92
00:03:50,800 --> 00:03:53,466
more involved in that process of caring

93
00:03:53,466 --> 00:03:55,500
for the gargoyle garden is a way

94
00:03:55,500 --> 00:03:57,700
of bringing people into closer contact with

95
00:03:57,700 --> 00:04:00,800
nature, If possible, if you're costing

96
00:04:00,800 --> 00:04:03,666
a project, always budget for the management

97
00:04:03,700 --> 00:04:05,466
in that project as well. What

98
00:04:05,466 --> 00:04:06,933
we're looking to do is looking to

99
00:04:07,000 --> 00:04:08,400
grow gardeners

100
00:04:08,666 --> 00:04:09,900
as well as gardens

