1
00:00:00,833 --> 00:00:03,200
Finally i want to give you

2
00:00:03,233 --> 00:00:05,633
an overview of creating a forest garden

3
00:00:05,666 --> 00:00:08,366
I think there's essentially four different

4
00:00:08,366 --> 00:00:12,133
steps to creating a forest garden: surveying

5
00:00:12,400 --> 00:00:14,133
the site in the first place,

6
00:00:14,400 --> 00:00:18,166
designing the garden and then creating the

7
00:00:18,200 --> 00:00:20,166
structure, landscaping the

8
00:00:20,266 --> 00:00:22,600
structural elements in the garden and

9
00:00:22,600 --> 00:00:25,900
then finally, working on area by area

10
00:00:25,900 --> 00:00:27,800
so you're landscaping one area at a

11
00:00:27,800 --> 00:00:30,400
time. Very often if you try and

12
00:00:30,400 --> 00:00:32,400
do everything all at the same time,

13
00:00:32,466 --> 00:00:35,366
it can be overwhelming. First of

14
00:00:35,366 --> 00:00:39,166
all, survey. I do have a whole

15
00:00:39,433 --> 00:00:40,866
forest garden checklist

16
00:00:40,866 --> 00:00:43,466
for the surveying process but basically

17
00:00:43,466 --> 00:00:44,766
there's four different things you want to

18
00:00:44,766 --> 00:00:47,000
be looking at. There's the orientation, this

19
00:00:47,000 --> 00:00:49,300
is a screenshot for an application called

20
00:00:49,300 --> 00:00:52,933
Sun Surveyor, and this is what direction

21
00:00:53,100 --> 00:00:55,133
does your garden face, how much light

22
00:00:55,133 --> 00:00:56,900
does your garden get and how much

23
00:00:56,900 --> 00:00:58,666
light does your garden get at different

24
00:00:58,666 --> 00:01:00,466
times of year. That's why an application

25
00:01:00,466 --> 00:01:02,633
like Sun Surveyor is so useful because

26
00:01:02,900 --> 00:01:03,866
it will let you know where the

27
00:01:03,866 --> 00:01:05,200
sun is in the sky at different

28
00:01:05,200 --> 00:01:06,933
times of year and then you get

29
00:01:06,933 --> 00:01:08,366
an idea of what plants to put

30
00:01:08,366 --> 00:01:10,900
where. You want to be looking at the

31
00:01:10,966 --> 00:01:13,966
elements so this is the wind

32
00:01:13,966 --> 00:01:18,566
direction, temperatures, the climate, rainfall, what kind

33
00:01:18,566 --> 00:01:20,633
of soil you have, testing your soil,

34
00:01:20,633 --> 00:01:22,166
how much clay is in the soil,

35
00:01:22,200 --> 00:01:25,166
all these basic elemental things but it's

36
00:01:25,366 --> 00:01:26,966
rarely really important to get your hands

37
00:01:26,966 --> 00:01:29,533
dirty into the soil and find out

38
00:01:29,866 --> 00:01:32,733
what your site is like.

39
00:01:32,766 --> 00:01:36,300
Existing features, whether they're public footpaths and

40
00:01:36,333 --> 00:01:40,200
gates and access points, tracks, power lines,

41
00:01:40,199 --> 00:01:44,833
water pipes, all that existing structural foundations

42
00:01:44,833 --> 00:01:47,400
for greenhouses, all those kind of things.

43
00:01:47,866 --> 00:01:49,933
Finally, the neighbours, where you

44
00:01:49,933 --> 00:01:52,666
are situated, what kind of things you're

45
00:01:52,666 --> 00:01:54,433
near, what things you want to

46
00:01:54,433 --> 00:01:56,266
screen or keep out.

47
00:01:56,266 --> 00:01:58,766
The second stage in the process is the

48
00:01:58,900 --> 00:02:00,166
design stage

49
00:02:00,966 --> 00:02:01,966
and I'm going to break this down

50
00:02:01,966 --> 00:02:04,500
into five different parts. First of all

51
00:02:04,500 --> 00:02:05,833
you want to be looking at structures,

52
00:02:05,833 --> 00:02:08,300
the big heavy things that aren't

53
00:02:08,300 --> 00:02:09,633
going move anyway, so this is your

54
00:02:09,633 --> 00:02:13,433
polytunnels, youre potting sheds, your compost heaps,

55
00:02:13,466 --> 00:02:16,400
to a degree, any kind of

56
00:02:16,400 --> 00:02:18,933
watering pipes and all that kind of

57
00:02:19,066 --> 00:02:20,400
infrastructure stuff

58
00:02:21,533 --> 00:02:23,866
Then the paths, now the paths

59
00:02:23,866 --> 00:02:26,233
of really important because they create

60
00:02:26,533 --> 00:02:29,533
access to the different structures and they

61
00:02:29,533 --> 00:02:32,266
also create the areas as well in

62
00:02:32,266 --> 00:02:34,900
the garden. Paths are fundamental for

63
00:02:34,900 --> 00:02:37,133
me when I'm designing a garden, paths

64
00:02:37,133 --> 00:02:41,266
are absolutely fundamental part of that process

65
00:02:41,266 --> 00:02:44,200
of creating different parts and different

66
00:02:44,766 --> 00:02:45,600
areas to go

67
00:02:47,133 --> 00:02:48,933
Third one is windbreaks, mentioned these

68
00:02:48,933 --> 00:02:51,333
before so so important to get the

69
00:02:51,333 --> 00:02:53,600
windbreaks in and design these windbreaks

70
00:02:53,966 --> 00:02:56,566
so that they're smaller in the south facing

71
00:02:56,866 --> 00:02:57,866
because you don't want to block

72
00:02:57,866 --> 00:03:00,766
out any sun, taller on the northern

73
00:03:00,766 --> 00:03:04,366
boundary and then the east and west boundaries,

74
00:03:04,366 --> 00:03:06,700
they can be in the middle.

75
00:03:06,700 --> 00:03:08,733
The fourth part of the design is the canopies,

76
00:03:08,733 --> 00:03:10,433
the tree layers, do bear in mind the

77
00:03:10,433 --> 00:03:12,933
tree spacing, how big the trees get.

78
00:03:13,333 --> 00:03:15,500
For fruit trees this is determined by

79
00:03:15,500 --> 00:03:17,900
a rootstock, so the rootstock determines

80
00:03:17,900 --> 00:03:19,066
the height and the vigour of a

81
00:03:19,066 --> 00:03:20,533
tree, so make sure you get the

82
00:03:20,533 --> 00:03:22,366
right rootstock, make sure you're getting the

83
00:03:22,366 --> 00:03:24,233
right sized tree for that space, that

84
00:03:24,233 --> 00:03:25,600
you have a quarter to half of

85
00:03:25,600 --> 00:03:27,133
the average canopy diameter

86
00:03:27,133 --> 00:03:29,566
around each tree so you're not

87
00:03:29,566 --> 00:03:32,133
shading out all the understorey.

88
00:03:32,233 --> 00:03:35,400
Finally, design the shrubs and the

89
00:03:35,433 --> 00:03:37,466
ground cover and herbaceous perennials and

90
00:03:37,466 --> 00:03:41,766
the perennial vegetables, design that around the areas

91
00:03:41,766 --> 00:03:43,066
that you've got and the

92
00:03:43,066 --> 00:03:44,400
trees that you have planned out.

93
00:03:45,633 --> 00:03:47,333
The third part of creating a forest

94
00:03:47,333 --> 00:03:50,366
garden is landscaping the structure, putting in

95
00:03:50,366 --> 00:03:53,100
the structural elements and that's the paths,

96
00:03:54,133 --> 00:03:57,100
the structures and the canopy and the

97
00:03:57,100 --> 00:03:59,000
windbreaks. With the paths, if you've

98
00:03:59,000 --> 00:04:00,466
got a large forest garden then you're

99
00:04:00,466 --> 00:04:02,233
going to have grass paths. The smaller

100
00:04:02,233 --> 00:04:03,766
the garden you have, the higher

101
00:04:03,766 --> 00:04:07,100
traffic areas, you'll have hard standing paths.

102
00:04:07,100 --> 00:04:09,300
For structures, it really depends what

103
00:04:09,300 --> 00:04:10,633
you're putting in but that would be

104
00:04:11,266 --> 00:04:15,200
wildlife ponds or polytunnel, greenhouse, potting shed

105
00:04:16,533 --> 00:04:18,833
seating areas. Try and get them all

106
00:04:18,833 --> 00:04:19,899
done at once. If you're getting a

107
00:04:19,899 --> 00:04:22,133
mini digger in for example to put

108
00:04:22,133 --> 00:04:24,033
in a wildlife pond it might be

109
00:04:24,033 --> 00:04:25,800
an idea to use the mini digger

110
00:04:25,800 --> 00:04:27,966
to level out areas

111
00:04:27,966 --> 00:04:29,733
for seating as well.

112
00:04:29,733 --> 00:04:33,233
Then there's the canopy, planting out the trees

113
00:04:33,600 --> 00:04:36,133
This is always best done in winter

114
00:04:36,133 --> 00:04:38,333
using bare-root trees and don't be tempted

115
00:04:38,333 --> 00:04:40,200
to use really large trees, you're

116
00:04:40,200 --> 00:04:41,666
better off with one year

117
00:04:41,666 --> 00:04:43,200
old trees because they are quicker to

118
00:04:43,200 --> 00:04:44,666
get established and will catch up with

119
00:04:44,666 --> 00:04:46,066
more mature trees anyway

120
00:04:47,000 --> 00:04:49,900
And then planting out the windbreaks.

121
00:04:50,600 --> 00:04:53,466
The fourth and final part of creating

122
00:04:53,466 --> 00:04:55,833
a forest garden is to landscape the

123
00:04:55,833 --> 00:04:59,000
areas. What is called soft landscaping and really

124
00:04:59,200 --> 00:05:01,233
once you've got your main elements in

125
00:05:01,400 --> 00:05:02,666
you can sit back a

126
00:05:02,666 --> 00:05:04,900
little bit and take your time, don't

127
00:05:04,900 --> 00:05:06,966
feel you have to do the whole garden

128
00:05:06,966 --> 00:05:08,033
all at once, this is a

129
00:05:08,033 --> 00:05:10,700
process which can take quite a few years

130
00:05:10,700 --> 00:05:14,166
Concentrate on area by area,

131
00:05:14,200 --> 00:05:17,633
deturf, plant your shrubs and your ground

132
00:05:17,633 --> 00:05:20,866
cover plants and then use a mulch

133
00:05:21,100 --> 00:05:23,500
like bark mulch is preferable and then

134
00:05:23,500 --> 00:05:26,100
a temporary ground cover, I use White

135
00:05:26,100 --> 00:05:27,133
Mustard and

136
00:05:27,166 --> 00:05:28,566
that just suppresses the weeds for a

137
00:05:28,566 --> 00:05:31,000
year. Once you've planted up

138
00:05:31,000 --> 00:05:32,500
the shrubs and the ground cover in one

139
00:05:32,500 --> 00:05:34,800
area, then move on to

140
00:05:34,800 --> 00:05:36,400
the next area rather than try and

141
00:05:36,400 --> 00:05:37,700
do it all at the same time.

142
00:05:38,000 --> 00:05:40,500
And there we go, that's the end

143
00:05:40,500 --> 00:05:42,600
of this Forest Garden Primer. I've put

144
00:05:42,600 --> 00:05:45,066
down a few resources for the class as

145
00:05:45,066 --> 00:05:47,233
well so there's Martin Crawford's book

146
00:05:47,266 --> 00:05:50,000
'Creating a Forest Garden', there's the garden survey

147
00:05:50,000 --> 00:05:52,366
checklist which is a PDF, there is

148
00:05:52,366 --> 00:05:54,866
a Forest Garden Spreadsheet which lists all

149
00:05:54,866 --> 00:05:58,433
the plants in Martin Crawford's book and

150
00:05:58,500 --> 00:06:01,700
it's freely available online and it's sortable

151
00:06:01,700 --> 00:06:03,500
as well so you can find plants

152
00:06:03,500 --> 00:06:06,166
by different types. There's the Sun Surveyor

153
00:06:06,166 --> 00:06:07,433
app which i mentioned which is

154
00:06:07,433 --> 00:06:09,766
very useful when you're surveying your garden

155
00:06:10,100 --> 00:06:12,666
Plants For A Future website, absolutely brilliant resource

156
00:06:12,733 --> 00:06:14,700
loads and loads of information about useful

157
00:06:14,700 --> 00:06:16,599
plants from all over the world

158
00:06:16,599 --> 00:06:20,400
Plant Atlas, really good resource, botanically

159
00:06:20,400 --> 00:06:24,200
inclined but fantastic resource for looking

160
00:06:24,200 --> 00:06:25,766
at different UK native plants and what

161
00:06:25,766 --> 00:06:27,599
their distribution is in the UK as well.

162
00:06:27,600 --> 00:06:29,300
Then there's the RHS

163
00:06:29,300 --> 00:06:31,933
Plant Finder which again is a really

164
00:06:31,933 --> 00:06:34,966
good resource biased more towards ornamentals and different

165
00:06:34,966 --> 00:06:36,933
cultivars but still very very useful and

166
00:06:36,933 --> 00:06:37,833
they do have a lot of good

167
00:06:37,900 --> 00:06:39,466
good information on there as well.

168
00:06:39,466 --> 00:06:41,000
There is a full list on my resources

169
00:06:41,000 --> 00:06:44,533
page here natureworks.org.uk/resources

170
00:06:44,533 --> 00:06:45,900
And there we go

171
00:06:45,900 --> 00:06:47,633
thank you very much for watching

172
00:06:47,800 --> 00:06:49,266
and I shall see you again soon

