1
00:00:01,033 --> 00:00:03,433
So, the third part, what characterises the

2
00:00:03,433 --> 00:00:06,366
forest garden. There are five different

3
00:00:06,366 --> 00:00:08,833
characteristics and by the way this is

4
00:00:08,833 --> 00:00:11,566
an illustration for Martin Crawford's book, 'Creating

5
00:00:11,633 --> 00:00:14,100
a Forest Garden' and it shows the

6
00:00:14,100 --> 00:00:16,233
further right you go and the

7
00:00:16,233 --> 00:00:18,700
more intense the agriculture and the more

8
00:00:18,700 --> 00:00:20,166
energy, the less diversity

9
00:00:20,166 --> 00:00:21,566
and the more fragility there is.

10
00:00:21,566 --> 00:00:24,400
So, far right is arable cultivation, if you're

11
00:00:24,400 --> 00:00:27,066
growing annual crops, you're using a lot of pesticides

12
00:00:27,066 --> 00:00:29,800
a lot of artificial fertilisers and you're ploughing

13
00:00:29,800 --> 00:00:31,133
the soil up as well

14
00:00:31,700 --> 00:00:33,633
And so that's pretty

15
00:00:34,100 --> 00:00:36,466
bad just in terms of sustainability.

16
00:00:36,533 --> 00:00:38,600
Then  you have pasture but don't forget, with

17
00:00:38,600 --> 00:00:40,833
livestock, you're also having to

18
00:00:40,900 --> 00:00:43,200
import a load of feed for the

19
00:00:43,200 --> 00:00:45,866
livestock as well. Then there's an orchard

20
00:00:46,533 --> 00:00:47,633
which you can see here is being

21
00:00:47,800 --> 00:00:49,733
grazed by sheep and then on the

22
00:00:49,800 --> 00:00:51,833
edge of the woodland and wild nature

23
00:00:51,833 --> 00:00:54,066
in a cool temperate climate, on

24
00:00:54,066 --> 00:00:56,000
the edge of that is a forest garden

25
00:00:56,666 --> 00:00:59,500
There are five characteristics for a

26
00:00:59,500 --> 00:01:00,933
forest garden. The first one is that

27
00:01:00,933 --> 00:01:03,200
it's sustainable, that it provides it's own

28
00:01:03,200 --> 00:01:05,933
fertility, you don't have to import fertility into

29
00:01:05,933 --> 00:01:06,466
the site.

30
00:01:07,133 --> 00:01:09,433
It's also providing its own pest control

31
00:01:09,433 --> 00:01:11,933
as well, by having a balanced ecosystem

32
00:01:12,400 --> 00:01:16,300
This photograph is of a comfrey

33
00:01:16,333 --> 00:01:19,466
being chopped and dropped on the base

34
00:01:19,466 --> 00:01:20,300
of a fruit tree

35
00:01:21,666 --> 00:01:24,133
The forest garden is also productive, there's

36
00:01:24,133 --> 00:01:25,900
a huge range of different crops that

37
00:01:25,900 --> 00:01:27,333
you can grow in a forest garden

38
00:01:27,333 --> 00:01:30,633
ranging from timber to bamboo, to fruits, nuts

39
00:01:31,400 --> 00:01:33,766
vegetables, all sorts of stuff and this

40
00:01:33,766 --> 00:01:36,400
is a Cornus capitata, Bentham's Cornel

41
00:01:36,800 --> 00:01:39,466
and tropical flavored fruits

42
00:01:41,133 --> 00:01:43,666
Really really importantly, a forest garden

43
00:01:43,700 --> 00:01:46,666
is wildlife friendly, is that it's built

44
00:01:46,700 --> 00:01:48,933
into the very fabric of the forest

45
00:01:48,933 --> 00:01:51,933
garden because you want the wildlife to

46
00:01:52,000 --> 00:01:53,866
be the pest control. For example this

47
00:01:53,900 --> 00:01:55,233
hoverfly, the larvae

48
00:01:55,266 --> 00:01:58,433
eat loads and loads of aphids and

49
00:01:58,433 --> 00:01:59,600
other insects

50
00:02:01,033 --> 00:02:03,900
Another characteristic is that a forest

51
00:02:03,900 --> 00:02:07,200
garden is grown in layers, so you're using

52
00:02:07,200 --> 00:02:09,466
the seven different layers, from

53
00:02:09,466 --> 00:02:11,200
the tree down to the

54
00:02:11,200 --> 00:02:15,166
smaller trees, shrubs and herbaceous perennials,

55
00:02:15,166 --> 00:02:19,300
ground cover, climbers, fungi so all

56
00:02:19,300 --> 00:02:20,633
the available space, all

57
00:02:20,633 --> 00:02:22,866
the ecological niches are being failed

58
00:02:23,800 --> 00:02:28,000
Finally, it's perennial and

59
00:02:28,833 --> 00:02:31,266
mostly, this is because perennial

60
00:02:31,266 --> 00:02:34,300
plants and perennial vegetables require less watering

61
00:02:34,300 --> 00:02:37,600
and less maintenance. But the self seeding

62
00:02:37,666 --> 00:02:39,200
annuals that look after themselves,

63
00:02:39,300 --> 00:02:42,200
for example Nasturtiums, are welcome in as

