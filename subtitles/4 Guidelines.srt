1
00:00:00,933 --> 00:00:02,866
For the fourth section, we're looking at

2
00:00:02,866 --> 00:00:05,933
helpful guidelines and I think there's four

3
00:00:05,933 --> 00:00:08,800
really useful, helpful guidelines that you

4
00:00:08,800 --> 00:00:10,666
can have when creating a forest garden

5
00:00:11,266 --> 00:00:13,066
The first one is a windbreak, getting

6
00:00:13,066 --> 00:00:15,266
you windbreaks established is super important

7
00:00:15,533 --> 00:00:17,266
and this is because you want to

8
00:00:17,266 --> 00:00:20,066
protect those young plants from

9
00:00:20,166 --> 00:00:22,700
the wind and the more protection they

10
00:00:22,700 --> 00:00:24,200
have the faster they will grow and

11
00:00:24,200 --> 00:00:26,666
the faster your forest garden will get established.

12
00:00:26,666 --> 00:00:28,733
Now the windbreaks in the

13
00:00:28,733 --> 00:00:30,033
first instance

14
00:00:31,333 --> 00:00:33,200
you should be planting a windbreak hedge,

15
00:00:33,200 --> 00:00:35,166
like you have here, this is a

16
00:00:35,166 --> 00:00:38,600
Yellow Dogwood, which is Cornus stolonifera flavarimea

17
00:00:38,600 --> 00:00:41,266
I think, but you can also

18
00:00:41,266 --> 00:00:43,933
use dead hedges, I'm a massive proponent

19
00:00:43,966 --> 00:00:46,166
of dead hedges. This is just four

20
00:00:46,166 --> 00:00:47,466
posts in the ground and you fill

21
00:00:47,466 --> 00:00:50,166
it with brash and prunings and then

22
00:00:50,166 --> 00:00:51,333
you put that in front of each

23
00:00:51,366 --> 00:00:53,566
specimen tree and that provides

24
00:00:53,566 --> 00:00:56,466
additional protection until such time the

25
00:00:56,466 --> 00:00:58,800
windbreaks have got established and by

26
00:00:58,800 --> 00:01:01,433
then the dead hedges would have decomposed.

27
00:01:01,433 --> 00:01:05,833
The second guideline is tree spacing

28
00:01:05,900 --> 00:01:07,933
This is really important, not to cram

29
00:01:07,933 --> 00:01:10,766
in too many trees into the garden

30
00:01:11,233 --> 00:01:13,466
The same principles apply when you're planting

31
00:01:13,466 --> 00:01:15,466
out shrubs but it's most important for trees,

32
00:01:15,466 --> 00:01:17,333
because trees are actually harder to move

33
00:01:17,333 --> 00:01:19,966
once they get established. The rule

34
00:01:19,966 --> 00:01:21,533
of thumb, it's a quarter to a

35
00:01:21,533 --> 00:01:25,000
half of the average canopy diameter is

36
00:01:25,033 --> 00:01:27,600
what you want as the spacing between trees

37
00:01:29,200 --> 00:01:32,633
The third guideline is to have a

38
00:01:32,666 --> 00:01:36,700
living ground cover. Always keep the ground covered

39
00:01:37,133 --> 00:01:39,166
In the first instance you'll be using

40
00:01:39,166 --> 00:01:40,866
a bark mulch or wood chip mulch,

41
00:01:40,900 --> 00:01:42,766
whatever material you've got to hand,

42
00:01:42,766 --> 00:01:45,600
to provide that ground cover  but once the plants

43
00:01:45,600 --> 00:01:48,166
get established and that can be specific

44
00:01:48,166 --> 00:01:48,966
ground cover plants

45
00:01:49,000 --> 00:01:50,866
like this Ground Ivy, Glechoma

46
00:01:50,866 --> 00:01:53,300
hederacea, which works really well

47
00:01:53,300 --> 00:01:54,133
here in West Wales

48
00:01:55,233 --> 00:01:56,866
or it can be the herbaceous perennials

49
00:01:56,866 --> 00:01:59,033
that grow and fill that space

50
00:01:59,033 --> 00:02:01,033
up as well but always always always

51
00:02:01,033 --> 00:02:04,066
keep the ground covered with living plants

52
00:02:04,833 --> 00:02:07,233
Finally nutrients. It's always a

53
00:02:07,266 --> 00:02:09,433
really good idea to calculate a nutrient

54
00:02:09,433 --> 00:02:11,600
budget in Martin Crawford's book in chapter

55
00:02:11,600 --> 00:02:15,366
six it goes through this process and

56
00:02:15,366 --> 00:02:17,833
test the soil as well for the

57
00:02:17,866 --> 00:02:20,400
nutrients and the pH and then

58
00:02:20,400 --> 00:02:21,833
you're making sure that you've got enough

59
00:02:21,833 --> 00:02:25,466
what are called "system plants", so comfrey and

60
00:02:25,533 --> 00:02:28,366
nitrogen fixing plants, in the garden to

61
00:02:28,400 --> 00:02:30,033
provide that fertility

