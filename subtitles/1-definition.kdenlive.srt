1
00:00:06,033 --> 00:00:08,266
Greetings and welcome my name is Jake Rayson

2
00:00:08,266 --> 00:00:10,000
I am a wildlife and food forest

3
00:00:10,000 --> 00:00:11,766
garden designer  and this is

4
00:00:11,800 --> 00:00:14,000
the first in my series of classes

5
00:00:14,266 --> 00:00:16,533
A Forest Garden Primer, this gives you

6
00:00:16,533 --> 00:00:18,633
an overview of what a forest garden is,

7
00:00:18,833 --> 00:00:21,366
why you’d want one and the

8
00:00:21,433 --> 00:00:23,700
the process in creating one as well

9
00:00:23,966 --> 00:00:25,200
Before we get started

10
00:00:25,300 --> 00:00:26,500
there’s just a couple of things

11
00:00:26,500 --> 00:00:28,466
I’d like to point out. Firstly, the

12
00:00:28,466 --> 00:00:31,133
slides are always available online

13
00:00:31,133 --> 00:00:34,833
natureworks.org.uk/slides/primer

14
00:00:34,833 --> 00:00:35,966
and if you press

15
00:00:35,966 --> 00:00:38,233
`p` you get to see the notes as well

16
00:00:38,233 --> 00:00:39,533
There’s lots of extra links and

17
00:00:39,533 --> 00:00:41,233
bits of information in there too

18
00:00:42,700 --> 00:00:44,466
And now, an outline of the class

19
00:00:44,466 --> 00:00:47,700
First of all, forest garden definition. Secondly,

20
00:00:48,066 --> 00:00:51,066
why forest gardening, why it’s so important now

21
00:00:51,066 --> 00:00:54,400
Thirdly, what characterises a forest garden.

22
00:00:54,633 --> 00:00:57,300
Fourth one is helpful guidelines, some simple

23
00:00:57,300 --> 00:00:59,933
ideas. Fifth one is key principles

24
00:00:59,933 --> 00:01:02,166
behind forest gardening

25
00:01:02,166 --> 00:01:04,599
and then finally I’ll round up with

26
00:01:04,599 --> 00:01:06,600
an overview of creating a forest garden

27
00:01:06,600 --> 00:01:09,200
the simple steps in that process

28
00:01:09,933 --> 00:01:12,500
So, a forest garden definition. You’re very

29
00:01:12,500 --> 00:01:16,600
lucky, I have given you three different definitions here.

30
00:01:16,600 --> 00:01:18,000
The first one. just to get

31
00:01:18,000 --> 00:01:19,666
a handle on it really, if you

32
00:01:19,666 --> 00:01:21,933
think of an orchard and then think

33
00:01:21,933 --> 00:01:23,733
of an orchard that's been planted for

34
00:01:23,766 --> 00:01:26,666
wildlife, and that's a really really good start.

35
00:01:26,666 --> 00:01:27,966
So you can imagine, you've got the

36
00:01:27,966 --> 00:01:30,000
trees, you've got windbreaks, you've got

37
00:01:30,000 --> 00:01:32,266
native plants in the mix and it's

38
00:01:32,266 --> 00:01:34,933
all designed around wildlife. And it’s a self

39
00:01:34,966 --> 00:01:40,033
nourishing wildlife orchard, underplanted with edible shrubs

40
00:01:40,133 --> 00:01:42,100
and perennial vegetables. You can get the

41
00:01:42,100 --> 00:01:44,533
idea of the trees and the edible shrubs

42
00:01:44,666 --> 00:01:45,833
and the perennial vegetables

43
00:01:47,700 --> 00:01:50,566
The second definition is a woodland age

44
00:01:50,900 --> 00:01:52,966
a forest garden emulates a woodland edge

45
00:01:53,033 --> 00:01:56,666
layered with edible perennials, self-sustaining nutrients

46
00:01:56,866 --> 00:01:59,466
and pest control. And it’s worth pointing

47
00:01:59,466 --> 00:02:01,533
out that a forest garden really

48
00:02:01,533 --> 00:02:03,766
only applies in this sense

49
00:02:03,833 --> 00:02:06,066
to a cool temper and warmer climates

50
00:02:06,733 --> 00:02:10,233
In other drier or higher

51
00:02:10,300 --> 00:02:12,000
climates, like mountain climates, there won't be

52
00:02:12,000 --> 00:02:12,933
that tree cover

53
00:02:13,833 --> 00:02:16,266
The third one, a shorthand is

54
00:02:16,300 --> 00:02:17,866
to think of a forest garden as

55
00:02:17,866 --> 00:02:20,566
an edible ecosystem. The whole point is,

56
00:02:20,566 --> 00:02:23,300
you are growing an ecosystem that just so

57
00:02:23,400 --> 00:02:27,166
happens to be edible. Growing edible crops

58
00:02:27,266 --> 00:02:28,666
in a wildlife garden

