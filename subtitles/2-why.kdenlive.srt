1
00:00:01,400 --> 00:00:05,100
So, why forest gardening, what's so important about it?

2
00:00:06,033 --> 00:00:06,833
Four words

3
00:00:08,266 --> 00:00:10,766
Nature and Climate Crisis

4
00:00:11,266 --> 00:00:15,200
There is just so much happening at

5
00:00:15,200 --> 00:00:15,766
the moment

6
00:00:16,366 --> 00:00:19,000
and it can be overwhelming but this

7
00:00:19,033 --> 00:00:22,666
is the main driver for growing forest

8
00:00:22,666 --> 00:00:23,366
gardens

9
00:00:25,666 --> 00:00:28,833
and this is a wildfire in Canada

10
00:00:29,266 --> 00:00:30,966
in 2009 but you

11
00:00:30,966 --> 00:00:31,733
get the idea

12
00:00:35,566 --> 00:00:38,600
So the response to the climate crisis,

13
00:00:38,600 --> 00:00:41,766
which is climate resilience, creating systems that

14
00:00:41,766 --> 00:00:44,533
are resilient to the climate change, is

15
00:00:44,566 --> 00:00:46,300
also the solution

16
00:00:47,166 --> 00:00:49,300
So krisis apparently means that

17
00:00:49,300 --> 00:00:51,166
point, it's a Greek word, it's that

18
00:00:51,166 --> 00:00:52,866
point at which the patient either

19
00:00:52,866 --> 00:00:54,966
dies or makes a recovery and it

20
00:00:54,966 --> 00:00:56,866
is a time of change. But there

21
00:00:56,866 --> 00:00:59,400
is hope in this crisis because that

22
00:00:59,466 --> 00:01:01,733
can be change for better ways of

23
00:01:01,800 --> 00:01:05,300
creating fairer and more robust and resilient

24
00:01:05,300 --> 00:01:05,933
systems

25
00:01:07,333 --> 00:01:08,966
So. what can i do is a

26
00:01:08,966 --> 00:01:11,466
typical refrain. There is a vast amount

27
00:01:11,466 --> 00:01:12,933
that you can do. There's a great

28
00:01:12,933 --> 00:01:15,333
deal of hope and an awful lot of people doing

29
00:01:15,333 --> 00:01:18,333
some marvellous work out there. In terms

30
00:01:18,333 --> 00:01:21,000
of a forest garden, you grow edible

31
00:01:21,000 --> 00:01:24,000
crops with nature, use native plants where

32
00:01:24,000 --> 00:01:26,866
possible and create wildlife habitat

33
00:01:26,900 --> 00:01:30,933
to support depleted ecosystems and this all can

34
00:01:30,933 --> 00:01:32,933
be summed up in my work

35
00:01:33,466 --> 00:01:36,800
I'm pushing for wildlife allotments, it's essentially

36
00:01:36,800 --> 00:01:39,533
community forest gardens is what we're looking at

37
00:01:39,533 --> 00:01:41,266
There's a great deal of other

38
00:01:41,266 --> 00:01:42,666
stuff that you can do as well

39
00:01:42,833 --> 00:01:45,666
looking at changing governance, the process of

40
00:01:45,666 --> 00:01:47,033
politics by using Community

41
00:01:47,033 --> 00:01:50,266
Assemblies and Citizens Assemblies, start

42
00:01:50,266 --> 00:01:53,333
decommodifying food and become an activist, become

43
00:01:53,400 --> 00:01:55,566
a climate activist because we all really do

44
00:01:55,566 --> 00:01:56,766
need to get involved

